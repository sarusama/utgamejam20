extends TextureRect

export (NodePath) var OWNER
export (Color) var On_Hover = Color(1,1,1)

# Called when the node enters the scene tree for the first time.
func _ready():
	if OWNER and is_instance_valid(get_node(OWNER)):
		OWNER = get_node(OWNER)
	else:
		print("Interactable item reference node didn't work out?\nDouble check the OWNER is pointing to a node")
		print("This node will be queued for deletion now")
		queue_free()
		
#Will be called when mouse is hovering over the node-texture
func _on_InteractableItem_mouse_entered():
#	print("entered")
	modulate = On_Hover

#Will be called when mouse is no longer hovering over the node-texture
func _on_InteractableItem_mouse_exited():
#	print("exited")
	modulate = Color(1,1,1)

#Will call the "custom_click" function name given when clicked
func _on_InteractableItem_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
#		print(OWNER.has_method("_physics_process"))
		if OWNER.has_method("custom_click"):
			OWNER.custom_click()
		else:
			print(OWNER.get_path(), " doesn't have the function custom_click")
