extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var state = 0
onready var player = get_node("Player")
var mothership
var dog
export var velShip = 120
export var velDog = 100
export var path = "res://city.tscn"
var dogbutton

var timer = null



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if state == 1:
		mothership.position.x += -velShip * delta
		if mothership.position.x <= 5:
			state = 2
	if state == 2:
		# add the timer on the first time through state 2
		# didn't want to add a new state so close to the end and break it all
		if timer == null:
			timer = Timer.new()
			timer.connect("timeout", self, "stagechange")
			add_child(timer)
			timer.one_shot = true
			timer.wait_time = 3.2
			timer.start()
		# blink the pre-beam
		var normTime = 1.0 - (timer.time_left / timer.wait_time);
		var fasterTime = pow(normTime, 3.0)
		var blinkRate = 10.0 * PI
		var period = cos(fasterTime * blinkRate)
		mothership.get_child(2).visible = (period >= 0.0)
	if state == 3:
		mothership.get_child(2).visible = false
		mothership.get_child(1).visible = true
		dog.position.y += -velDog * delta
		
		if dog.position.y <= -1000:
			state = 4
	if state == 4:
		mothership.get_child(1).visible = false
		mothership.position.x += velShip * delta
		if mothership.position.x >= 1950:
			state = 5
	if state == 5:
		mothership.free()
		state = 6
	if state == 6:
		player.controlable = true
		player.visible = true
		state = 7
	if state == 7 and player.position.x > 1760:
		get_node("Robobo").custom_click()
		state = 8
	if state == 8 and player.position.x > 3075:
		Fade.change_scene(path)

func stagechange():
	state = 3

func _ready():
	mothership = get_node("house/mothership")
	dog = get_node("house/Dog")
	
	state = 1
	mothership.get_child(0).play()
