extends Path2D
class_name EaseSpline

export var TimeSpan = 1.0

var mDomainScale  = Vector2()
var mDomainOffset = Vector2()
var mBakedLength  = 0.0
#var mPointTimes   = []

func evaluate(time: float) -> float:
	var normalizedTime = clamp(time / TimeSpan, 0.0, 1.0);
	var bakedOffset    = normalizedTime * mBakedLength
	var evaluatedPos   = curve.interpolate_baked(bakedOffset, true)
	var normalizedY    = evaluatedPos.y * mDomainScale.y + mDomainOffset.y
	return normalizedY


# Bake the spline on construction and cache the evaluation parameters
func _ready():
	var pointCount  = curve.get_point_count()
	var minPoint    = curve.get_point_position(0);
	var maxPoint    = curve.get_point_position(pointCount-1)
	var domainRange = maxPoint - minPoint
	
	mDomainScale  = Vector2(1.0, 1.0) / domainRange
	mDomainOffset = -minPoint         / domainRange
	
	mBakedLength = curve.get_baked_length()
	pass
