shader_type canvas_item;

uniform vec2 Amplitude;// = vec2(2, 8);

void vertex()
{
//	VERTEX += cos(TIME) * Amplitude;
}

uniform vec2 BogusColor;

uniform float AspectRatio = 1.77; // ~ 16:9
uniform float NoiseTile = 1.0;

uniform sampler2D NoiseTex: hint_black;
uniform float NoiseAmplitude = 0.0;

vec4 indirect_lookup(float tiling, vec2 baseUv, sampler2D baseTex)
{
	vec2 noiseUv = baseUv * tiling;
	noiseUv.y *= AspectRatio;
	vec2 uvOffset = texture(NoiseTex, noiseUv).rg * 2.0 - 1.0;
	
	vec2 uv = baseUv + uvOffset * NoiseAmplitude;
	
	vec4 tex = texture(baseTex, uv);
	return tex;
}

void fragment()
{
//	COLOR = vec4(UV, 0.0, 1.0);
//	vec4 sum = indirect_lookup(NoiseTile, UV, TEXTURE);
	const int NUMSAMPLES = 4;
	vec4 col = vec4(0.0);
	for (int i=0; i<NUMSAMPLES; ++i)
	{
		float tileRate = 0.5;
		col += indirect_lookup(NoiseTile*tileRate*(float(i)+1.0), UV, TEXTURE);
	}
	col /= float(NUMSAMPLES);
/*
	vec2 noiseUv = UV * NoiseTile;
	noiseUv.y *= AspectRatio;
	vec2 uvOffset = texture(NoiseTex, noiseUv).rg * 2.0 - 1.0;
	
	vec2 uv = UV + uvOffset * NoiseAmplitude;
	
	vec4 tex = texture(TEXTURE, uv);
*/
	col.rg *= BogusColor;
	COLOR = col;
}