extends Sprite


# Declare member variables here. Examples:
var elapsedTime = 0
var initial = Vector2()

const SPAN = 2.0

# Called when the node enters the scene tree for the first time.
func _ready():
	initial = Vector2(0, -200)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	elapsedTime += delta
	elapsedTime = fmod(elapsedTime, SPAN)
	var normalized = $"../AccelSpline".evaluate(elapsedTime)
	position.x = initial.x + (elapsedTime/SPAN)*100.0
	position.y = initial.y - normalized*100.0
	pass
