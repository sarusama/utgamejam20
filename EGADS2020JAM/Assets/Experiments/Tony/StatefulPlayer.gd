extends KinematicBody2D

enum EGroundLocomotionState {Idle, Accel, Steady, Turn, Decel}

export var mSpeed                         = 30.0
export var mAcceleration                  = 1.0
export var mDeceleration                  = 1.0
export var mVerticalGroundLocomotionScale = 1

var mVelocity = Vector2()

var mElapsedInState = 0.0
var mGroundState    = EGroundLocomotionState.Idle

func resolve_desired_dir() -> Vector2:
	# default to "no direction"
	var dir = Vector2(0.0, 0.0)
	
	# process input that drives X axis direction
	if Input.is_action_pressed("ui_right"):
		dir.x = 1.0
	elif Input.is_action_pressed("ui_left"):
		dir.x = -1.0
	
	# process input that drives Y axis direction
	if Input.is_action_pressed("ui_up"):
		dir.y = -1.0
	elif Input.is_action_pressed("ui_down"):
		dir.y = 1.0
	
	return dir

func switch_state(newState: int):
	mGroundState    = newState
	mElapsedInState = 0.0

func process_ground_locomotion(delta: float, desiredDir: Vector2):
	# accumulate the time we've spent in our current state
	mElapsedInState += delta
	
	match mGroundState:
		EGroundLocomotionState.Idle:
			if desiredDir.length() != 0:
				# need to accelerate into the desired direction
				switch_state(EGroundLocomotionState.Accel)
				process_ground_locomotion(delta, desiredDir)
				
		EGroundLocomotionState.Accel:
			var accelScale = 1.0 #$AccelSpline.evaluate(mElapsedInState)
			# potentially move 
			if (accelScale >= 1.0):
				print("blarg")
	pass

func _physics_process(delta):
	# process the input to figure out what the user wants the player to do
	var desiredDir = resolve_desired_dir()
	
	# process ground locomotion
	# if playerState == EPlayerState.OnGround
	process_ground_locomotion(delta, desiredDir)
	
	move_and_slide(mVelocity)
