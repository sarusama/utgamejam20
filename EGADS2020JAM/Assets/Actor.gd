extends KinematicBody2D
class_name Actor

export var Gravity = 1000
export var MaxVelocity  = Vector2(1000.0,1000.0)
export var Acceleration = 1000.0
export var Dampening    = 1.0

export var VerticalLocomotionScale = 1.0

var mVelocity = Vector2()

const EPSILON = 0.1

func update_velocity(delta: float, impulse: Vector2):
	var VelDelta
	if (impulse.length() > EPSILON):
		impulse *= delta
		var accel = impulse * Acceleration
		mVelocity += accel
	else:
		mVelocity.x *= Dampening
	if !is_on_floor():
		mVelocity.y += Gravity * delta
	mVelocity.x = clamp(mVelocity.x, -MaxVelocity.x, MaxVelocity.x)
	mVelocity.y = clamp(mVelocity.y, -MaxVelocity.y, MaxVelocity.y)
	print(impulse)
	pass
