extends Actor
export var jump_force = 2000
func resolve_user_impulse() -> Vector2:
	# default to "no direction"
	var dir = Vector2(0.0, 0.0)
	
	# process input that drives X axis direction
	if Input.is_action_pressed("ui_right"):
		dir.x = 1.0
	elif Input.is_action_pressed("ui_left"):
		dir.x = -1.0
	
	# process input that drives Y axis direction
	if Input.is_action_pressed("ui_up") and is_on_floor():
		dir.y = -jump_force
	elif Input.is_action_pressed("ui_down"):
		dir.y = 1.0
	
	return dir

func _physics_process(delta):
	var impulse = resolve_user_impulse()
	update_velocity(delta, impulse)
	

	move_and_slide(mVelocity, Vector2(0,-1), true)
	
