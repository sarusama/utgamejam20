extends KinematicBody2D

export (int) var run_speed = 100
export (int) var jump_speed = -400
export (int) var gravity = 1200
onready var anim = get_node("AnimatedSprite")
var velocity = Vector2()
var jumping = false
var timer
var jump_squat = false
var controlable = false

func _ready():
	visible = false
	

	

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed('ui_right')
	var left = Input.is_action_pressed('ui_left')
	var jump = Input.is_action_just_pressed('ui_select')

	if jump and is_on_floor():
		timer = Timer.new()
		
		timer.connect("timeout", self, "jumpsquat")
		add_child(timer)
		timer.one_shot = true
		timer.wait_time = .05
		timer.start()
		jumping = true
		jump_squat = true
	if right:
		if is_on_floor():
			anim.animation = "run"
		anim.flip_h = false
		velocity.x += run_speed
	if left:
		if is_on_floor():
			anim.animation = "run"
		anim.flip_h = true
		velocity.x -= run_speed
		
	if !is_on_floor():
		anim.animation = "inair"
	
	if is_on_floor() and !right and !left:
		anim.animation = "idle"
	
	if jump_squat:
		anim.animation = "jumpup"

func jumpsquat():
	velocity.y = jump_speed
	jump_squat = false

func _physics_process(delta):
	if(controlable):
		get_input()
	velocity.y += gravity * delta
	if jumping and is_on_floor():
		jumping = false
	
	velocity = move_and_slide(velocity, Vector2(0, -1))
