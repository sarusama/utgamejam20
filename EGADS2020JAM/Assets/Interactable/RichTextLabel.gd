extends RichTextLabel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var f = 0
var text_index = 0
export var frames_per_letter = 3
var playing = false
export var test_section = []

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible_characters = 0
	self.bbcode_text = test_section[text_index]
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(playing):
		f = f + 1
		if (f % frames_per_letter) == 0:
			self.visible_characters = self.visible_characters + 1
		if (self.bbcode_text.length()  + 10 < (f / frames_per_letter)) and text_index < test_section.size()-1:
			text_index = text_index + 1
			self.bbcode_text = test_section[text_index]
			f = 0
			self.visible_characters = 0
			
func custom_click():
	playing = true
	
