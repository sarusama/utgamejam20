extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal scene_changed()

onready var animation_player = $AnimationPlayer
onready var black = $Control/black

func change_scene(path, delay = 0.5):
	yield(get_tree().create_timer(delay), "timeout")
	animation_player.play("fade")
	yield(animation_player, "animation_finished")
	var result = get_tree().change_scene(path)
	assert(result == OK)
	animation_player.play_backwards("fade")
	emit_signal("scene_changed")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
