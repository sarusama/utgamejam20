extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var player
# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_child(2)
	player.visible = true
	player.controlable = true
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player.position.x > 320 and player.position.y < -440:
		Fade.change_scene("res://final.tscn")
