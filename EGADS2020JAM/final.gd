extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var state = 0
onready var player = get_node("Player")
onready var evil = get_node("Bad boy")
onready var text = get_node("TextScene/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	player.controlable = true
	player.visible = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if state == 0 and player.position.x > -200:
		player.controlable = false
		player.velocity.x = 0
		player.velocity.y = 0
		player.anim.animation = "idle"
		state = 1
	if state == 1:
		player.get_child(0).offset.x += 100 * delta
		if player.get_child(0).offset.x > 95:
			state = 2
	if state == 2:
		text.custom_click()
		if text.finished:
			state = 3
	if state == 3:
		evil.animation = "shot"
		state = 4
	if state == 4:
		yield(get_tree().create_timer(1.5), "timeout")
		state = 5
	if state == 5:
		player.anim.animation = "reveal"
		yield(get_tree().create_timer(2), "timeout")
		state = 6
	if state == 6:
		text.last_words()
		state = 7
	
